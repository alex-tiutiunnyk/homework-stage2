import { controls } from '../../constants/controls';

export const events = [];

export async function fight(firstFighter, secondFighter) {
  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;

  return new Promise((resolve) => {
    bindEvents(firstFighter,secondFighter, resolve);
  });
}

export function getDamage(attacker, defender) {
  let atackPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);
  return  atackPower > blockPower ? atackPower - blockPower : 0;
}

export function getHitPower(fighter) {
  return fighter.attack;
}

export function getBlockPower(fighter) {
  return fighter.defense;
}

function bindEvents(firstFighter, secondFighter,resolve){
  events.push(document.addEventListener('keypress',(event)=>{
    const keyName = 'Key' + event.key.toUpperCase();
    let damage = 0;
    switch (keyName) {
      case controls.PlayerOneAttack:
        firstFighter.isBlock=false;
        if(!secondFighter.isBlock){
          damage = getDamage(firstFighter,secondFighter);
          secondFighter.currentHealth -= damage;
        }
        updateHealth(firstFighter,secondFighter);
        checkGameIsOver(firstFighter,secondFighter, resolve);
        break;
      case controls.PlayerTwoAttack:
        secondFighter.isBlock=false;
        if(!firstFighter.isBlock){
          damage = getDamage(secondFighter,firstFighter);
          firstFighter.currentHealth -= damage;
        }
        updateHealth(firstFighter,secondFighter);
        checkGameIsOver(secondFighter,firstFighter, resolve);
        break;
      case controls.PlayerOneBlock:
        firstFighter.isBlock=true;
        break;
      case controls.PlayerTwoBlock:
        secondFighter.isBlock=true;
        break;
      default:
        break;
    }
  }));

}

function checkGameIsOver(firstPlayer,secondPlayer,resolve){
  if(firstPlayer.currentHealth <=0)
    resolve(secondPlayer);
  else if(secondPlayer.currentHealth <=0)
    resolve(firstPlayer);
}

function updateHealth(firstFighter,secondFighter){
  document.getElementById('left-fighter-indicator').style.width = (firstFighter.currentHealth * 100 / firstFighter.health) + "%";
  document.getElementById('right-fighter-indicator').style.width = secondFighter.currentHealth * 100 / secondFighter.health+ "%";
}