import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  let res = "";
  if(fighter != undefined){
  const imageElement = createFighterImage(fighter);
  let id = `<input type='hidden' name="id" value="${fighter['name']}" />`;
  let name = `<div class='item name'>Name: ${fighter['name']}</div>`;
  let health = `<div class='item health'>Health: ${fighter['health']}</div>`;
  let attack = `<div class='item attack'>Attack: ${fighter['attack']}</div>`;
  let defense = `<div class='item defense'>Defense: ${fighter['defense']}</div>`;
  res += id + name+health + attack + defense;
  let wrapContainer =document.createElement('div'); 
  wrapContainer.className = 'wrap';
  wrapContainer.innerHTML = res;
  fighterElement.append(wrapContainer);
  fighterElement.append(imageElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
