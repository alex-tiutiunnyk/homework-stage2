import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    let fileName = `resources/api/details/fighter/${id}.json`;
    return fetch(fileName, { headers: { "Content-Type": "application/json; charset=utf-8" }})
    .then(res => res.json())
    .catch(err => {
      alert("sorry, there are no results for your search")
  });
  }
}


export const fighterService = new FighterService();
